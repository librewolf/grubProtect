# grubProtect

#### This script allows users to protect grub boot loader from compromise and set a security layer on it.

## Usage
```sh
~$ git clone https://git.disroot.org/librewolf/grubProtect 

~$ cd grubProtect && chmod +x grubProtect.sh

~$ sudo ./grubProtect.sh
```

### License

`Distributed under the GPL V3 License. See LICENSE for more information`

